export const icons = {
  user: require('./user.svg'),
  image: require('./image.svg'),
  exit: require('./exit.svg')
};
