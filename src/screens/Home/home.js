import React, { useEffect } from 'react';
import * as s from './styled-home';
import { Projeto } from './../../components/';
import mock from './mock.json';
import { withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Home = () => {
  const user = useSelector(state => state.user);
  return (
    <s.Container>
      {mock.map(projeto => (
        <Projeto key={projeto.id} user={user.nome} pass={user.pass} projeto={projeto} />
      ))}
    </s.Container>
  );
};

export default withRouter(Home);
