import Home from './Home/home';
import Compra from './Compra/compra';
import Login from './Login/login';
import Produto from './Produto/produto';

export { Home, Compra, Login, Produto };
