import React, { useState } from 'react';
import * as s from './styled-login';
import { Button } from './../../components/';
import { Link, withRouter } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

const Login = props => {
  const dispatch = useDispatch();
  const teste = useSelector(state => state);

  const [user, setUser] = useState('');
  const [pass, setPass] = useState('');

  const addUser = () => {
    props.history.push('/home');
    return {
      type: 'SET_USER',
      user,
      pass,
      logado: true
    };
  };

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        dispatch(addUser(user, pass));
      }}
    >
      <s.Container>
        <s.Login>
          <s.Text>WallPaipe</s.Text>
          <input
            type="text"
            value={user}
            placeholder={'Usuário'}
            onChange={e => setUser(e.target.value)}
          />
          <input
            type="password"
            value={pass}
            placeholder={'Senha secreta'}
            onChange={e => setPass(e.target.value)}
          />
          <Button text={'Entrar'} type={'submit'} />
        </s.Login>
      </s.Container>
    </form>
  );
};

export default withRouter(Login);
