import React from "react";
import { Header, Button } from "./../../components/";
import * as s from "./styled-produto";
import { icons } from "./../../assets";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

const Produto = props => {
  const { nome, descricao, id } = props.projeto;
  return (
    <s.Container key={id}>
      <s.Image src={icons.image} />
      <s.AboutBox>
        <s.TextBox>
          <h1>{nome}</h1>
          <h3 style={{ color: "#989898" }}>{descricao}</h3>
        </s.TextBox>
        {!props.noBuy && (
          <Link to="/compra">
            <Button text={"Comprar"} />
          </Link>
        )}
      </s.AboutBox>
    </s.Container>
  );
};

export default Produto;
