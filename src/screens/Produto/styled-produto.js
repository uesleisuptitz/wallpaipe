import styled from "styled-components";

export const Container = styled.div`
  flex: 1;
  display: flex;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
  margin: 20px;
`;

export const Body = styled.div`
  display: flex;
  flex: 1;
  padding: 10px;
  justify-content: space-between;
`;

export const AboutBox = styled.div`
  flex: 1;
`;

export const Text = styled.label`
  color: ${props => props.color || "#000"};
  font-size: ${props => props.size || "18px"};
  font-weight: 500;
`;

export const TextBox = styled.div`
  display: flex;
  justify-content: space-around;
  flex-direction: column;
`;

export const ButtonBox = styled.div`
  display: flex;
  justify-content: center;
`;

export const Image = styled.img`
  display: flex;
  width: 200px;
  height: 200px;
`;
