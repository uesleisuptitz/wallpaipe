import React, { useState } from "react";
import { Produto } from "..";
import * as s from "./styled-compra";
import { Button } from "../../components";
import { useDispatch, useSelector } from "react-redux";

const Compra = props => {
  const user = useSelector(state => state.user);
  const projeto = useSelector(state => state.compra);
  const dispatch = useDispatch();

  const [compra, setCompra] = useState({
    nome: user.nome,
    idProjeto: projeto.id || "",
    nomeProjeto: projeto.nome || "",
    endereco: "",
    cartao: ""
  });

  const finalizar = () => {
    dispatch({
      type: "SUM_COMPRADOS"
    });
  };

  return (
    <s.Container>
      <h1>Comprar projeto {projeto.nome}</h1>
      <hr />
      <Produto projeto={projeto} noBuy />
      <s.InputContainer>
        <s.InputTitle>Nome</s.InputTitle>
        <input
          type="text"
          style={{ height: "30px", padding: "10px" }}
          value={compra.nome}
          readOnly
        />
      </s.InputContainer>
      <s.InputContainer>
        <s.InputTitle>Projeto</s.InputTitle>
        <input
          type="text"
          style={{ height: "30px", padding: "10px" }}
          value={compra.nomeProjeto}
          readOnly
        />
      </s.InputContainer>
      <s.InputContainer>
        <s.InputTitle>Endereço</s.InputTitle>
        <input
          type="text"
          style={{ height: "30px", padding: "10px" }}
          value={compra.endereco}
          onChange={e => setCompra({ ...compra, endereco: e.target.value })}
        />
      </s.InputContainer>
      <s.InputContainer>
        <s.InputTitle>Cartão</s.InputTitle>
        <input
          type="text"
          style={{ height: "30px", padding: "10px" }}
          value={compra.cartao}
          onChange={e => setCompra({ ...compra, cartao: e.target.value })}
        />
      </s.InputContainer>
      <div style={{ margin: "20px" }}>
        <Button text="Finalizar" onClick={() => finalizar()} />
      </div>
    </s.Container>
  );
};

export default Compra;
