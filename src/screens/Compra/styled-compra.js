import styled from "styled-components";

export const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const InputTitle = styled.label`
  margin-right: 10px;
`;
export const InputContainer = styled.div`
  flex: 1;
  display: flex;
  justify-content: space-between;
  width: 500px;
  align-items: center;
`;
