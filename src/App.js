import React from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import { Home, Login, Produto, Compra } from "./screens/index";
import GlobalStyle from "./utils/GlobalStyle";
import { Header } from "./components";
import { useSelector } from "react-redux";
const App = props => {
  const user = useSelector(state => state.user);
  return (
    <>
      <Header />
      <GlobalStyle />
      <Switch>
        {!user.nome ? (
          <>
            <Route
              path="/login"
              component={() => <Login history={props.history} />}
            />
            <Redirect to="/login" />
          </>
        ) : (
          <>
            <Redirect to="/home" />
            <Route path="/home" component={() => <Home />} />
            <Route path="/produto" component={() => <Produto />} />
            <Route path="/compra" component={() => <Compra />} />
          </>
        )}
      </Switch>
    </>
  );
};

export default withRouter(App);
