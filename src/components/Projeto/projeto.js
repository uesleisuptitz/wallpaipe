import React from "react";
import * as s from "./styled-projeto";
import { withRouter } from "react-router-dom";
import { icons } from "./../../assets";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "./../";

const Projeto = props => {
  const dispatch = useDispatch();
  const { nome, descricao, id } = props.projeto;

  const comprar = () => {
    dispatch({
      type: "SET_COMPRA",
      compra: { nome, descricao, id }
    });
    props.history.push("/compra");
  };

  return (
    <s.Container key={id}>
      <s.Header>
        <s.Text size={`14px`} color={"#fff"}>
          {nome}
        </s.Text>
      </s.Header>
      <s.Body>
        <s.Image src={icons.image} size={"60px"} />
        <s.Text size={`13px`}>{descricao}</s.Text>
      </s.Body>
      <s.Footer>
        <Button text={"Comprar"} onClick={() => comprar()} />
      </s.Footer>
    </s.Container>
  );
};

export default withRouter(Projeto);
