import styled from "styled-components";

export const Container = styled.div`
  background: #f8f8f8;
  width: 200px;
  height: 275px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  border-radius: 10px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
  margin: 8px;
`;

export const Header = styled.div`
  background: #9b59b6;
  display: flex;
  flex: 1;
  border-radius: 10px 10px 0 0;
  justify-content: center;
  align-items: center;
`;

export const Body = styled.div`
  display: flex;
  flex: 2;
  padding: 10px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const Footer = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const Text = styled.label`
  color: ${props => props.color || "#000"};
  font-size: ${props => props.size || "18px"};
  font-weight: 700;
`;

export const Image = styled.img`
  width: ${props => props.size || "18px"};
  height: ${props => props.size || "22px"};
`;
