import styled from 'styled-components';

export const Button = styled.button`
  font-family: 'Muli';
  font-weight: 700;
  width: 150px;
  height: 25px;
  border: none;
  text-decoration: none;
  cursor: pointer;
  background: #9b59b6;
  color: #fff;
  border-radius: 5px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
`;
