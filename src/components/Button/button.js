import React from "react";
import * as s from "./styled-button";

const Button = ({ text, type, onClick }) => {
  return (
    <s.Button type={type} onClick={onClick}>
      {text}
    </s.Button>
  );
};

export default Button;
