import React from "react";
import * as s from "./styled-header";
import { Link } from "react-router-dom";
import { icons } from "./../../assets";
import { useSelector, useDispatch } from "react-redux";

const Header = () => {
  const comprados = useSelector(state => state.comprados);
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  return (
    <s.Header>
      <Link to="/home" style={{ textDecoration: "none" }}>
        <s.Text style={{ cursor: "pointer" }}>WallPaipe</s.Text>
      </Link>
      <s.Item>
        <s.Avatar>
          <s.TextBox>
            {user.nome ? (
              <>
                <s.Text size={`14px`}>
                  {user.nome}, {comprados || 0} compras hoje!
                </s.Text>
              </>
            ) : (
              <>
                <s.Text size={`14px`}>sem dados</s.Text>
              </>
            )}
          </s.TextBox>
          <s.Image src={icons.user} size={"35px"} />
        </s.Avatar>
        <Link to="/login" onClick={() => dispatch({ type: "SET_USER" })}>
          <s.Image src={icons.exit} size={"35px"} />
        </Link>
      </s.Item>
    </s.Header>
  );
};

export default Header;
