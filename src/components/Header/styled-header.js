import styled from 'styled-components';

export const Header = styled.div`
  background: #f8f8f8;
  padding: 0 5px 0 5px;
  height: 50px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-radius: 5px;
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
`;

export const Text = styled.label`
  font-size: ${props => props.size || '18px'};
  font-weight: 700;
`;

export const TextBox = styled.div`
  padding-right: 5px;
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

export const Avatar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const Image = styled.img`
  width: ${props => props.size || '18px'};
  height: ${props => props.size || '22px'};
`;

export const Item = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
