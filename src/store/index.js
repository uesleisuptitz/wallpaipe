import { createStore } from "redux";

const INITIAL_STATE = {
  user: {
    nome: "",
    senha: ""
  },
  compra: null,
  comprados: 0,
  logado: false
};

const reducer = (state = INITIAL_STATE, action) => {
  console.log(action);
  if (action.type === "SET_USER") {
    return {
      ...state,
      user: { nome: action.user, senha: action.pass }
    };
  } else if (action.type === "SET_COMPRA") {
    return {
      ...state,
      compra: action.compra
    };
  } else if (action.type === "SUM_COMPRADOS") {
    return {
      ...state,
      comprados: state.comprados + 1
    };
  }
  return state;
};

const store = createStore(reducer);

export default store;
